resource "azurerm_virtual_network" "vnet-terraform" {
  name                = "vnet-terraform"
  resource_group_name = azurerm_resource_group.proevolua-terraform.name
  location            = azurerm_resource_group.proevolua-terraform.location
  address_space       = ["10.0.0.0/16"]
}