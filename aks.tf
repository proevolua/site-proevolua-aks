resource "azurerm_kubernetes_cluster" "proevolua" {
  name                = "proevolua-aks"
  resource_group_name = azurerm_resource_group.proevolua-terraform.name
  location            = azurerm_resource_group.proevolua-terraform.location
  dns_prefix          = "proevoluaaks"

  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_D2_v2"
  }

  identity {
    type = "SystemAssigned"
  }

  tags = {
    Environment = "Develop"
  }
}

output "client_certificate" {
  value     = azurerm_kubernetes_cluster.proevolua.kube_config.0.client_certificate
  sensitive = true
}

output "kube_config" {
  value = azurerm_kubernetes_cluster.proevolua.kube_config_raw

  sensitive = true
}