# Create a resource group
resource "azurerm_resource_group" "proevolua-terraform" {
  name     = "proevolua-terraform"
  location = "East US"
}